# dashboards-gitlab-com

This project uses the same queries used in http://dashboards.gitlab.com/ to visualize metrics using the
[GitLab Prometheus Integration](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html).

## How to view metrics

1. On the project sidebar, click on `Operations`
1. Click on `Metrics`
1. You can switch between dashboards using the dashboards dropdown on the top left corner

### Manual Updates

The dashboard definitions are recorded in `.gitlab/dashboards/sla-dashboard.yml` and any
adjustments to the "Weighted Availability Score - GitLab.com" require manual changes
to properly calculate the score (e.g. the denominator for a proper average).
